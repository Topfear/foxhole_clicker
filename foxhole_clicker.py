import pyautogui
from pynput.keyboard import Key, Listener

running = True

delay = 0.5  # in seconds
f2_button = Key.f2
f3_button = Key.f3
f5_button = Key.f5
f10_button = Key.f10

lmb_button_play = False
lmb_button_pressed = False
scoped = False
x_pos = 0
y_pos = 0

def on_press(key):
    global running, lmb_button_play
    global x_pos, y_pos
    global lmb_button_pressed, scoped

    if key == f2_button:
        x_pos, y_pos = pyautogui.position()
        lmb_button_play = True
    if key == f3_button:
        x_pos, y_pos = pyautogui.position()
        pyautogui.mouseDown(button="left")
        lmb_button_pressed = True
    if key == f5_button:
        if scoped:
            pyautogui.mouseUp(button="right")
        else:
            pyautogui.mouseDown(button="right")
    if key == f10_button:
        running = False
        print("Выход")


def do_stuff():
    global lmb_button_play, lmb_button_pressed
    if not (lmb_button_play or lmb_button_pressed):
        return

    global x_pos, y_pos
    x, y = pyautogui.position()
    

    if (abs(x - x_pos) + abs(y - y_pos)) > 4:
        lmb_button_play = False
        lmb_button_pressed = False
        pyautogui.mouseUp(button="left")

    if lmb_button_play:
        pyautogui.click()


def display_controls():
    print("// Кликер для лисьей норы")
    print("// - Управление:")
    print("\t F2 = кликает левой кнопкой (двиньте мышкой чтобы прекратить клики)")
    print("\t F3 = удерживает левую кнопку мышы нажатой (двиньте мышкой чтобы прекратить клики)")
    print("\t F5 = удержание прицела (нажатие шифта отменяет прицел)")
    print("\t F10 = выйти")
    print("// - Управление в фоксхоле :")
    print("-----------------------------------------------------")


def main():
    display_controls()

    lis = Listener(on_press=on_press)
    lis.start()
    while running:
        do_stuff()
        pyautogui.sleep(delay)
    lis.stop()


if __name__ == "__main__":
    main()
